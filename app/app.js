import 'es6-shim';
import {App, IonicApp, Platform, MenuController} from 'ionic-angular';
import {StatusBar} from 'ionic-native';

// Own pages
import {HomePage} from './pages/home/home';
// import {CategoryDetailsPage} from './pages/category-details/category-details';

// add it as a provider in our root component which will make it available 
// through the application
// got all fct for notes, creditcards etc.
import {Data} from './providers/data/data';

@App({
  // templateUrl: 'build/pages/home/home.html',
  templateUrl: 'build/app.html',
  providers: [Data],
  // providers: [ Data, CreditcardData, NotesData ],
  config: {}
})
export class MyApp {
  static get parameters() {
    return [[IonicApp], [Platform], [MenuController]];
  }
 
  constructor(app, platform, menu) {
    // set up our app
    this.app = app;
    this.platform = platform;
    this.menu = menu;
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Welcome to the m-Wallet', component: HomePage },
      { content: 'Content m-Wallet', component: HomePage }
    ];

    // make ListPage the root (or first) page
    this.rootPage = HomePage;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  // openPage(page) {
  //   // close the menu when clicking a link from the menu
  //   this.menu.close();
  //   // navigate to the new page if it is not the current page
  //   let nav = this.app.getComponent('nav');
  //   nav.setRoot(page.component);
  // }

}