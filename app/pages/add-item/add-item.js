import 'es6-shim';
import {Page, NavController, NavParams} from 'ionic-angular';
import { NgForm, FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators, AbstractControl, OnInit } from 'angular2/common';
import {HomePage} from '../home/home';

// The control group is needed to connect the questions to the form later.
@Page({
  templateUrl: 'build/pages/add-item/add-item.html',
  directives: [FORM_DIRECTIVES]
})

export class AddItemPage {

  static get parameters(){
    return [ [NavController], [NavParams], [FormBuilder] ];
  }

  constructor(nav, navParams) {
    this.nav = nav;
    this.navParams = navParams;
    this.selectedCategory = navParams.data.homePage.selectedCategory;
    this.submitted = false;
    
    //Use the same form when navigated from viewItem(), Item got tapped
    this.viewIt = this.navParams.get('viewItem');
    this.sqlKey = this.navParams.get('sqlKey');
    console.log("add-item.js this.sqlKey");
    console.log(this.sqlKey);
    this.readOnly = false;
    this.editForm = false;
    this.ngDoCheck();

    // Type names MUST be .png name in www/img folder
    this.cardtypes = ['Visa', 'Mastercard', 'Amazon', 'Bank Transfer', 'Bitcoin', 'Discover', 'Maestro', 'Paypal'];
    this.creditcardmodel = {
      cardtype: 'this.cardtypes[0]',
      valueCardNr: '',
      valueOwner: '',
      valueValMonth: '',
      valueValYear: '',
      valueSecurityCode: '',
      balance: ''
    }
    
    this.articlemodel = {
      name: '',
      price: ''
    }
    
    // Type names MUST be .png name in www/img folder
    this.licencetypes = ['Personal ID', 'Driving Licence', 'Motorcycle', 'Other'];
    this.idmodel = {
      licencetype: 'this.licencetypes[0]',
      valueCardNr: '',
      valueOwner: '',
      valueValDate: '',
      valueSecurityCode: ''
    }
    this.showOtherLicencetypeInput = false;

    this.keysmodel = {
      name: '',
      note: ''
    }

    this.tickettypes = ['Bus', 'Train', 'Plane', 'Boat', 'Other ticket'];
    this.ticketmodel = {
      tickettype: 'this.tickettypes[0]',
      valueCardNr: '',
      valueOwner: '',
      valueValDate: ''
    }

    this.vouchertypes = ['Food', 'Shopping', 'Cinema', 'Travel', 'Other voucher'];
    this.vouchermodel = {
      vouchertype: 'this.vouchertypes[0]',
      valueShopname: '',
      valueMoney: '',
      valueCardNr: '',
      valueOwner: '',
      valueValDate: ''
    }

    this.notetypes = ['Reminder', 'Work', 'Recipe', 'Other note'];
    this.notemodel = {
      notetype: 'this.notetypes[0]',
      name: '',
      date: '',
      description: ''
    }

    //Fill the form with data from WebSql
    if (this.viewIt == true) {
      this.readOnly = true;
      this.index = this.navParams.get('index');

      this.creditcardmodel.cardtype = this.navParams.get('item').cardtype;
      this.creditcardmodel.valueCardNr = this.navParams.get('item').valueCardNr;
      this.creditcardmodel.valueOwner = this.navParams.get('item').valueOwner;
      this.creditcardmodel.valueValMonth = this.navParams.get('item').valueValMonth;
      this.creditcardmodel.valueValYear = this.navParams.get('item').valueValYear;
      this.creditcardmodel.valueSecurityCode = this.navParams.get('item').valueSecurityCode;
      this.creditcardmodel.balance = this.navParams.get('item').balance;
      
      this.articlemodel.name = this.navParams.get('item').name;
      this.articlemodel.price = this.navParams.get('item').price;

      this.idmodel.licencetype = this.navParams.get('item').licencetype;
      this.idmodel.otherlicencetype = this.navParams.get('item').otherlicencetype;
      this.idmodel.valueCardNr = this.navParams.get('item').valueCardNr;
      this.idmodel.valueOwner = this.navParams.get('item').valueOwner;
      this.idmodel.valueValMonth = this.navParams.get('item').valueValMonth;
      this.idmodel.valueValYear = this.navParams.get('item').valueValYear;
      this.idmodel.valueSecurityCode = this.navParams.get('item').valueSecurityCode;

      this.keysmodel.name = this.navParams.get('item').name;
      this.keysmodel.note = this.navParams.get('item').note;

      this.ticketmodel.tickettype = this.navParams.get('item').tickettype;
      this.ticketmodel.othertickettype = this.navParams.get('item').othertickettype;
      this.ticketmodel.valueCardNr = this.navParams.get('item').valueCardNr;
      this.ticketmodel.valueOwner = this.navParams.get('item').valueOwner;
      this.ticketmodel.valueValMonth = this.navParams.get('item').valueValMonth;
      this.ticketmodel.valueValYear = this.navParams.get('item').valueValYear;

      this.vouchermodel.vouchertype = this.navParams.get('item').vouchertype;
      this.vouchermodel.othervouchertype = this.navParams.get('item').othervouchertype;
      this.vouchermodel.valueShopname = this.navParams.get('item').valueShopname;
      this.vouchermodel.valueMoney = this.navParams.get('item').valueMoney;
      this.vouchermodel.valueCardNr = this.navParams.get('item').valueCardNr;
      this.vouchermodel.valueOwner = this.navParams.get('item').valueOwner;
      this.vouchermodel.valueValMonth = this.navParams.get('item').valueValMonth;
      this.vouchermodel.valueValYear = this.navParams.get('item').valueValYear;

      this.notemodel.notetype = this.navParams.get('item').notetype;
      this.notemodel.othernotetype = this.navParams.get('item').othernotetype;
      this.notemodel.name = this.navParams.get('item').name;
      this.notemodel.date = this.navParams.get('item').date;
      this.notemodel.description = this.navParams.get('item').description;
    }
    
  }

  // Lifecycle hook fct
  ngDoCheck() {}

  goToHomePage(event) {
    console.log('goToHomePage !!!');
    this.nav.push(HomePage, {
        // category: this.allCategorys[2],
        // categorys: this.allCategorys
    });
  }

  onSubmitSaveCard() {
    console.log(JSON.stringify(this.creditcardmodel));
    this.submitted = true; 
    let newCreditcard = this.creditcardmodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(newCreditcard, this.index);
    }

    this.navParams.get('homePage').saveItem(newCreditcard);
    this.nav.pop();
  }

  onSubmitSaveCash() {
    console.log(JSON.stringify(this.articlemodel));
    this.submitted = true; 
    let newArticle = this.articlemodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(newArticle, this.index);
    }

    this.navParams.get('homePage').saveItem(newArticle);
    this.nav.pop();
  }

  onSubmitSaveId() {
    console.log(JSON.stringify(this.idmodel));
    this.submitted = true; 
    let licenceIdItem = this.idmodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(licenceIdItem, this.index);
    }

    this.navParams.get('homePage').saveItem(licenceIdItem);
    this.nav.pop();
  }

  onSubmitSaveKey() {
    console.log(JSON.stringify(this.keysmodel));
    this.submitted = true; 
    let keyItem = this.keysmodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(keyItem, this.index);
    }

    this.navParams.get('homePage').saveItem(keyItem);
    this.nav.pop();
  }

  onEditCard() {
    this.readOnly = false;
    this.editForm = true;
    this.viewIt = false;
  }

  onSubmitSaveTicket() {
    console.log(JSON.stringify(this.ticketmodel));
    this.submitted = true; 
    let ticketItem = this.ticketmodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(ticketItem, this.index);
    }

    this.navParams.get('homePage').saveItem(ticketItem);
    this.nav.pop();
  }

  onEditTicket() {
    this.readOnly = false;
    this.editForm = true;
    this.viewIt = false; 
  }

  onSubmitSaveVoucher() {
    console.log(JSON.stringify(this.vouchermodel));
    this.submitted = true; 
    let voucherItem = this.vouchermodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(voucherItem, this.index);
    }

    this.navParams.get('homePage').saveItem(voucherItem);
    this.nav.pop();
  }

  onEditVoucher() {
    this.readOnly = false;
    this.editForm = true;
    this.viewIt = false; 
  }

  onSubmitSaveNote() {
    console.log(JSON.stringify(this.notemodel));
    this.submitted = true; 
    let noteItem = this.notemodel;

    if (this.editForm == true) {
      this.navParams.get('homePage').editItem(noteItem, this.index);
    }

    this.navParams.get('homePage').saveItem(noteItem);
    this.nav.pop();
  }

  onEditNote() {
    this.readOnly = false;
    this.editForm = true;
    this.viewIt = false; 
  }

}

