import {Page, NavController, NavParams} from 'ionic-angular';
import {AddItemPage} from '../add-item/add-item';
import {Data} from '../../providers/data/data';
import {NgZone, NgClass, OnInit} from 'angular2/core';
import {HomePage} from '../home/home';

@Page({
	templateUrl: 'build/pages/category-details/category-details.html'
})
export class CategoryDetailsPage {
	static get parameters() {
		return [[NavController], [NavParams], [Data], [NgZone]];
	}

	constructor(nav, navParams, dataService, zone) {
		this.nav = nav;
        this.navParams = navParams;
		// If we navigated to this page, we will have an category available as a nav param
        this.allCategorys = navParams.get('categorys');
        this.selectedCategory = navParams.get('category');
        this.dataService = dataService;
        this.zone = zone;
        this.items = [];
        this.item = {};
        this.sqlKey = this.selectedCategory.webSqlKey;
        this.negativeBalance = false;
        this.basketValue = 0;
        this.date = new Date();
        this.today = this.date.getDate() + '.' + (this.date.getMonth()+1) + '.' + this.date.getFullYear();
        // Get data from WebSql according to key
        console.log('INIT this.sqlKey');
        console.log(this.sqlKey);
        

        // if (this.sqlKey != this.allCategorys[2].webSqlKey) {
            this.dataService.getData(this.sqlKey).then((pSqlKey) => {
                if (pSqlKey){
                    this.zone.run(() => {	
                        this.items = JSON.parse(pSqlKey);
                        // console.log('INIT this.items dataService.getData');
                        // console.log(this.items);
                   });             
                }
            });
        // }

        /************************************************
        **************         CASH        **************
        /***********************************************/
        if (this.sqlKey == this.allCategorys[0].webSqlKey) {
            this.toggleArticlesText = "Show";
            this.creditcards = [];
            this.creditcardUsed = [];
            this.creditcardUsedExist = false;
            this.creditcardIndex = null;
            this.creditcardSqlKey = this.allCategorys[1].webSqlKey;
            // For toggle fcts
            this.showCreditCardSelection = false;
            this.showArticles = false;
            // this.showPaymentHistory = false;
            // Init fcts
            this.getCreditCards();
            this.getSelectedCreditcard();
            this.ngDoCheck();

            /********* For payment history *************/
            this.paymentHistoryData = [];
            this.getAllPaymentArticles();
        }

        /************************************************
        ***********         CREDITCARDS        **********
        /***********************************************/
        if (this.sqlKey == this.allCategorys[1].webSqlKey) {
            this.ngDoCheck();
        }
        /************************************************
        *********         PAYMENT HISTORY        ********
        /***********************************************/
        // if (this.sqlKey == this.allCategorys[2].webSqlKey || this.showPaymentHistory == true) {
        if (this.sqlKey == this.allCategorys[2].webSqlKey) {
            this.searchQuery = '';
            this.spentMoney = 0;
            this.initializeItems();
        }
        
    } // end constructor

    initializeItems() {
        // this.items = [
        //   'Amsterdam', 'Bogota', 'Buenos Aires', 'Cairo', 'Dhaka', 'Edinburgh', 'Geneva', 'Genoa', 'Glasglow', 'Hanoi', 'Hong Kong', 'Islamabad', 'Istanbul', 'Jakarta', 'Kiel', 'Kyoto', 'Le Havre', 'Lebanon', 'Lhasa', 'Lima', 'London', 'Los Angeles', 'Madrid', 'Manila', 'New York', 'Olympia', 'Oslo', 'Panama City', 'Peking', 'Philadelphia', 'San Francisco', 'Seoul', 'Taipeh', 'Tel Aviv', 'Tokio', 'Uelzen', 'Washington'
        // ];
        this.dataService.getData(this.sqlKey).then((pSqlKey) => {
            if (pSqlKey){
                this.calculateBasketValue();
                this.items = JSON.parse(pSqlKey);
                let iterItems = [];
                this.items.forEach( function(element, index) {
                    iterItems.push(
                        '<ion-row class="flexWrap">' + 
                        '<ion-col width-10 class="article-basket mgn-right-10">' + (index + 1) + '</ion-col>' + 
                        '<ion-col width-33>' + element.name + '</ion-col>' + 
                        '<ion-col width-25 class="primary">' + element.date + '</ion-col>' +
                        '<ion-col width-20><span class="dark pull-right price">' + element.price + '€</span></ion-col>' + 
                        '</ion-row>'
                        );
                });
                this.items = iterItems;
            }
        });

    }

    getItems(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();
        // set q to the value of the searchbar
        var q = searchbar.value;
        // if the value is an empty string don't filter the items
        if (q.trim() == '') {
          return;
        }
        var valEuroArray = [];
        this.items = this.items.filter((v) => {
          if (v.toLowerCase().indexOf(q.toLowerCase()) > -1) {
            console.log('FILTER v');
            console.log(v);
            var valEuro = v.match( 'price">(.*)€</span>' )[1];
            valEuroArray.push(valEuro);
            // this.basketValue = JSON.parse(valEuro) + JSON.parse(valEuro);
            console.log('this.basketValue ' + this.basketValue);
            console.log(valEuro);
            console.log(valEuroArray);
            var totalValEuro = 0;
            valEuroArray.forEach( function(element, index) {
                totalValEuro += JSON.parse(element);
            });
            this.basketValue = totalValEuro;
            return true;
          }
          return false;
        })
    }
    

    // Lifecycle hook fct
    ngDoCheck() {
        // CASH
        if (this.sqlKey == this.allCategorys[0].webSqlKey) {
            this.calculateBasketValue();
            if (this.creditcardUsed.balance < 0) {
                this.negativeBalance = true;
            } else {
                this.negativeBalance = false;
            }
        } // CREDITCARDS
        else if (this.sqlKey == this.allCategorys[1].webSqlKey) {
            if (this.item.balance < 0) {
                this.negativeBalance = true;
            } else {
                this.negativeBalance = false;
            }
        } //PAYMENT HISTORY
        else if (this.sqlKey == this.allCategorys[2].webSqlKey) {

        }
    }

    addItem(){
    	this.nav.push(AddItemPage, {
            homePage: this,
            viewItem: false,
            sqlKey: this.sqlKey
        });
    }

    getAllItems(paramSqlKey) {
        this.dataService.getData(paramSqlKey).then((pSqlKey) => {
            if (pSqlKey){
                this.zone.run(() => {   
                    this.items = JSON.parse(pSqlKey);
               });             
            }
        });
    }

    saveAllItems() {
    	this.dataService.saveData(this.items, this.sqlKey);
    }

    saveItem(item){
    	this.items.push(item);
    	this.dataService.saveData(this.items, this.sqlKey);
    }
    // Give nav all needed params
    viewItem(item, idx){
        console.log("viewItem this.sqlKey");
        console.log(this.sqlKey);
        this.nav.push(AddItemPage, {
          homePage: this,
          item: item,
          viewItem: true,
          index: idx,
          sqlKey: this.sqlKey
      });
    }

    removeItem(item, idx) {
    	this.items.splice(idx, 1);
    	this.saveAllItems();
    }

    editItem(item, idx) {
        this.items.splice(idx, 1);
        this.saveAllItems();
    }

    /************************************************
    **************         CASH        **************
    /***********************************************/
    calculateBasketValue() {
        let totalValue = 0;
        this.items.forEach( function(element, index) {
            totalValue += JSON.parse(element.price);
        });
        this.basketValue = totalValue;
        return this.basketValue;
    }

    calculatePaymentValue() {
    }

    toggleCardSelection() {
        if (this.showCreditCardSelection == false) {
            this.showCreditCardSelection = true;
        } else {
            this.showCreditCardSelection = false;
        }
    }

    toggleArticles() {
        if (this.showArticles == false) {
            this.showArticles = true;
            this.toggleArticlesText = "Hide";
        } else {
            this.showArticles = false;
            this.toggleArticlesText = "Show";
        }
        console.log(this.items);
    }

    getCreditCards() {
        this.dataService.getData(this.creditcardSqlKey).then((pSqlKey) => {
            if (pSqlKey){
                this.zone.run(() => {   
                    this.creditcards = JSON.parse(pSqlKey);
                });             
            }
        });
    }

    getSelectedCreditcard() {
        this.dataService.getData('cashcardused').then((pSqlKey) => {
            if (pSqlKey){
                this.zone.run(() => {   
                    this.creditcardUsed = JSON.parse(pSqlKey);
                    this.creditcardUsedExist = true;
                });             
            }
        });
    }

    goToHomePage(event) {
    console.log('goToHomePage !!!');
    this.nav.push(HomePage, {
        // category: this.allCategorys[2],
        // categorys: this.allCategorys
    });
  }

    saveSelectedCreditcard(idx) {
        this.creditcardUsed = this.creditcards[this.creditcardIndex];
        this.creditcardUsed.index = this.creditcardIndex;
        this.dataService.saveData(this.creditcardUsed, 'cashcardused');
        this.creditcardUsedExist = true;
    }

    goToPaymentHisory(event) {
        // this.showPaymentHistory = true;
        this.nav.push(CategoryDetailsPage, {
            category: this.allCategorys[2],
            categorys: this.allCategorys
        });
        // this.nav.pop();
        this.sqlKey = this.allCategorys[2].webSqlKey;
        this.getPaymentHistory();
    }

    getPaymentHistory() {
        this.dataService.getData(this.sqlKey).then((pSqlKey) => {
            if (pSqlKey){
                this.zone.run(() => {   
                    this.items = JSON.parse(pSqlKey);
                });             
            }
        });
    }

    updateCreditCardsCash() {
        // Remove from creditcards and add the new one
        this.creditcards.splice(this.creditcardUsed.index, 1, this.creditcardUsed);
        // Update/Save new creditcards
        this.dataService.saveData(this.creditcards, this.creditcardSqlKey);
        // Update/Save new used creditcard
        this.dataService.saveData(this.creditcardUsed, 'cashcardused');
    }

    buyArticle(item, idx) {
        this.creditcardUsed.balance -= item.price;
        item.date = this.today;
        this.saveArticleToPaymentHistory(item);
        // Remove article
        this.removeItem(item, idx);
        this.updateCreditCardsCash();
    }

    saveArticleToPaymentHistory(item) {
        console.log('saveArticleToPaymentHistory');
        this.paymentHistoryData.push(item); // Add new one
        this.dataService.saveData(this.paymentHistoryData, this.allCategorys[2].webSqlKey);
    }

    buyAll(items, today, paymentHistoryData) {
        let totalPrice = 0;
        // Take care of articles and assign them purchasing date
        items.forEach( function(element, index) {
            totalPrice += JSON.parse(element.price);
            element.date = today;
            // Replace old item with updated one that now got a date
            items.splice(index, 1, element);
            // Attach new element = item to array paymentHistoryData
            paymentHistoryData.push(element);
        });
        // Save all in payment history
        this.paymentHistoryData = paymentHistoryData;
        this.saveAllArticlesToPaymentHistory(); // Save all articles
        // // No more articles to display
        this.items = [];
        this.saveAllItems();
        // // Update creditcard used for payment
        this.creditcardUsed.balance -= totalPrice;
        this.updateCreditCardsCash();
    }

    saveAllArticlesToPaymentHistory() {
        this.dataService.saveData(this.paymentHistoryData, this.allCategorys[2].webSqlKey);
    }

    getAllPaymentArticles() {
        this.dataService.getData(this.allCategorys[2].webSqlKey).then((pSqlKey) => {
            if (pSqlKey){
                this.zone.run(() => {   
                    this.paymentHistoryData = JSON.parse(pSqlKey);
               });             
            }
        });
    }

    

} // End of page

