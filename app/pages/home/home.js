import {Page, NavController, NavParams} from 'ionic-angular';
import {CategoryDetailsPage} from '../category-details/category-details';
import {OnInit} from 'angular2/core';

@Page({
  templateUrl: 'build/pages/home/home.html',
})

export class HomePage {

  static get parameters(){
    return [[NavController], [NavParams]];
  }
  constructor(nav, navParams) {

    this.nav = nav;
    this.selectedCategory = navParams.get('category');
    this.categorys = [];

    this.categorys[0] = {
      title: 'Cash',
      note: 'Transferd cash from accounts, Quickpay, P2P Money Transfers',
      icon: 'money.png',
      webSqlKey: 'cash',
      addLabelText: 'Article'
    }

    this.categorys[1] = {
      title: 'Bank- & creditcards',
      note: 'Bank accounts, mobile banking, standing orders, P2P Money Transfers',
      icon: 'Other.png',
      webSqlKey: 'creditcards',
      addLabelText: 'Card',
    }

    this.categorys[2] = {
      title: 'Payment history',
      note: 'See all the transfers you made',
      icon: 'hourglass.png',
      webSqlKey: 'paymentHistory'
    }

    this.categorys[3] = {
      title: 'Ids & Licences',
      note: 'Personal Id, passport, driving licence & health care',
      icon: 'Personal ID.png',
      webSqlKey: 'personalIdsLicences',
      addLabelText: 'Personal Card'
    }

    this.categorys[4] = {
      title: 'Keys',
      note: 'Key Cards',
      icon: 'key.png',
      webSqlKey: 'keys',
      addLabelText: 'Key Card'
    }

    this.categorys[5] = {
      title: 'Tickets',
      note: 'Plane-, train-, bustickets etc',
      icon: 'Plane.png',
      webSqlKey: 'tickets',
      addLabelText: 'Ticket'
    }

    this.categorys[6] = {
      title: 'Vouchers',
      note: 'Collect your vouchers',
      icon: 'stack.png',
      webSqlKey: 'vouchers',
      addLabelText: 'Voucher'
    }

    this.categorys[7] = {
      title: 'Notes',
      note: 'Personal notes, shopping lists & recipes',
      icon: 'clipboard.png',
      webSqlKey: 'notes',
      addLabelText: 'Note'
    }

  }

  categoryTapped(event, category) {
   this.nav.push(CategoryDetailsPage, {
    category: category,
    categorys: this.categorys
  });
 }
}