import {Storage, SqlStorage} from 'ionic-angular';
import {Injectable} from 'angular2/core';
 
@Injectable()
export class Data {

  constructor(){
    // Create database
    this.storage = new Storage(SqlStorage, {name:'mWallet'});
    this.data = null;
    // Reset in case
    // this.storage.set('paymentHistory', '');
  }
 
  getData(pSqlKey) {
    // console.log('getData(pSqlKey)');
    // console.log(pSqlKey);
    return this.storage.get(pSqlKey);  
  }
 
  saveData(data, pSqlKey){
    data
    let newData = JSON.stringify(data);
    // Change this for a new key
    this.storage.set(pSqlKey, newData);
  }

}